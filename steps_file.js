// in this file you can append custom step methods to 'I' object

const validateI = (_I) => {
  if (typeof _I === 'undefined') throw Error('You must provide the "I" actor to run this async function')
}

module.exports = function () {
  return actor({
    
    // Define custom steps here, use 'this' to access default methods of I.
    // I inside async don´t work, used _I to ref it and is needed when calling methods from this file
    
    // TODO: optimize selectors hardcoded definitions inside method
    
    basicLogin: async (_I, userData) => {

      validateI(_I)

      if (!userData.username || !userData.password || !userData.name) throw Error('User data "username", "password" or "name" not well provided')

      _I.amOnPage('/')

      _I.clearStorage(_I)
        .then(()=>{
          _I.waitForInvisible('.spinner-container', 30)
          _I.fillField('.form-input[type=text][autocomplete=username]', userData.username)
          _I.fillField('//input[@type="password"]', secret(userData.password))
          _I.click('//form//button[@type="submit"]')
          _I.waitForText(userData.name + '!', 30)
          tryTo(() => {
            // close annoying survey
            _I.waitForElement('//div[@class="toast --survey"]//button[2]')
            _I.click('//div[@class="toast --survey"]//button[2]')
          })
        })
        .catch( e => console.log(!!e ? e.message || e.error || e : "erro..?"))
    },

    clearStorage: async (_I) => {

      validateI(_I)
      
      _I.executeScript(() => "localStorage.clear()")
        .then(() => {
          _I.clearCookie()
          _I.refreshPage()
        })
        .catch(e => console.log(!!e ? e.message || e.error || e : "erro..?"))
    },

    basicLogout: async (_I) => {

      validateI(_I)

      const logoutAction = async (_I) => {
        _I.waitForVisible('.profile-link', 30)
        _I.click('.profile-link')
        _I.click('.profile-content-link[href$="logout"]')
        _I.refreshPage() // TODO: hack - understand why logout loading spinner remains
        _I.waitForInvisible('.spinner-container', 30)
        _I.seeElement('.form-input[type=text][autocomplete=username]', 10)
        _I.say('logout ok')
      }

      await logoutAction(_I)

      // if (process.env.profile !== 'undefined') {
      //   logoutAction(_I)
      // } else {
      //   await _I.executeScript("window.location.href")
      //     .then((data) => {
      //       try {
      //         if (!!data && !data.includes('auth/signin')) {
      //           logoutAction(_I)
      //         } else {
      //           _I.say('nothing to do...')
      //         }
      //       } catch (e) {
      //         _I.say(`some error ocurred:\n${e}`)
      //       }
      //     }).catch(e => console.log(!!e ? e.message || e.error || e : "erro..?"))
      // }

    }

  });
}
