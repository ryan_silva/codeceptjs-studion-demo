module.exports = {
  browser: "firefox",
  //restart: false,
  //keepBrowserState: true,
  //keepCookies: true,
  url: "https://stqa1.qa.dotgroup.com.br",
  host: "hub-cloud.browserstack.com",
  user: process.env.BROWSERSTACK_USERNAME,
  key: process.env.BROWSERSTACK_ACCESS_KEY,
  //port: 443,
  //smartWait: 5000,
  //windowSize: "1366x600",
  //uniqueScreenshotNames: true,
  //basicAuth: { "username": "username", "password": "password" },
  // timeouts: {
  //   "script": 60000,
  //   "page load": 30000
  // },
  capabilities: {
    'bstack:options' : {
      "os" : "Windows",
      "osVersion" : "10",
      "resolution" : "1366x768",
      "local" : "true",
      "seleniumVersion" : "3.10.0"
      },
      "browserName" : "Firefox",
      "browserVersion" : "latest",
  }
}