module.exports = {
  browser: "chrome",
  //restart: false,
  //keepBrowserState: true,
  //keepCookies: true,
  url: require("../resources/data/test_data.json").SPA_BASE_URL,
  host: "hub-cloud.browserstack.com",
  user: process.env.BROWSERSTACK_USERNAME,
  key: process.env.BROWSERSTACK_ACCESS_KEY,
  //port: 443,
  //smartWait: 5000,
  //windowSize: "1366x600",
  //uniqueScreenshotNames: true,
  //basicAuth: { "username": "username", "password": "password" },
  // timeouts: {
  //   "script": 60000,
  //   "page load": 30000
  // },
  capabilities: {
    // using W3C version of capabilities, found in https://www.browserstack.com/automate/capabilities?tag=selenium-4 
    'bstack:options': {
      "idleTimeout": "300",
      "os": "Windows",
      "osVersion": "10",
      "resolution": "1366x768",
      "local": "true"
    },
    "browserName": "Chrome",
    "browserVersion": "latest",
  }
}