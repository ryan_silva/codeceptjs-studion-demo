module.exports = {
  browser: "chrome",
  restart: false,
  keepBrowserState: true,
  keepCookies: true,
  url: "https://stqa1.qa.dotgroup.com.br",
  smartWait: 5000,
  windowSize: "1366x600",
  uniqueScreenshotNames: true,
  //basicAuth: { "username": "username", "password": "password" },
  // timeouts: {
  //   "script": 60000,
  //   "page load": 30000
  // },
  capabilities: {
    chromeOptions: {
      args: [ "--headless", "--disable-gpu", "--no-sandbox" ]
    }
  }
}