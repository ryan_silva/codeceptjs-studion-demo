const { setHeadlessWhen } = require('@codeceptjs/configure');
const { devices } = require('playwright');

// turn on headless mode when running with HEADLESS=true environment variable
// (export) HEADLESS=true (&&) npx codeceptjs run
setHeadlessWhen(process.env.HEADLESS);

function defineHelpers() {
  // TODO: if another helper comes, you need to insert it here
  let helpers = {} // reset
  if (process.env.profile === 'undefined') {
    // default of the project is to run local and with playwright
    // but if is needed to run with browserstack, --profile should be used
    // TODO: improve profiles for playwright like webdriver
    helpers.Playwright = {
      url: require("./resources/data/test_data.json").SPA_BASE_URL,
      browser: 'chromium',
      show: true,
      emulate: devices['Desktop Chrome HiDPI'], // devices['iPhone 6'], https://codecept.io/playwright/#device-emulation
      // https://github.com/microsoft/playwright/blob/master/src/server/deviceDescriptorsSource.json
      // windowSize: '1366x600', // disable if emulate is used
      // video: false,
      // keepVideoForPassedTests: false,
      // trace: false,
      // keepTraceForPassedTests: false,
      waitForAction: 300, // ms
      pressKeyDelay: 3, // ms
      waitForTimeout: 10000, // @debug
      // TODO:  studion uses on-premise storage for images...
      //        this causes weird behavior in tests, if it goes to cdn/cloud, change this to false  
      // restart: false,
      // keepBrowserState: false,
      // keepCookies: false,
      // waitForNavigation: 'networkidle',
      chromium: {
        // channel: 'msedge', // or 'chrome'; see https://playwright.dev/docs/browsers/#google-chrome--microsoft-edge
        args: [ 
          // "--window-size=1366,600",
          "--browser-test"
        ]
      }
    }
  } else {
    // user profile informed by the --profile argument from CJS
    // if unexpected, failsafe to chrome locally, the default profile
    // TODO: support edge
    const profile = ['chrome', 'safari', 'firefox'].includes(process.env.profile) ? process.env.profile : 'default'
    helpers.WebDriver = require(`./configs/webdriver_${profile}_latest.js`)
  }
  
  helpers.Mochawesome = {
    uniqueScreenshotNames: "true"
  }

  helpers.REST = {
    endpoint: require('./resources/data/test_data.json').API_BASE_URL
  }

  return helpers
}

exports.config = {
  tests: 'tests/*_test.js',
  output: 'tests/output',
  helpers: defineHelpers(),
  include: {
    I: './steps_file.js',
    loginPage: './pages/loginPage.js',
    commonPage: './pages/commonPage.js'
  },
  bootstrap: null,
  rerun: {
    // run with run-rerun option to detect flaky tests
    minSuccess: 2,
    maxReruns: 4,
  },
  mocha: {
    reporterOptions: {
      reportDir: "tests/output"
    }
  },
  name: 'codeceptjs-studion-demo',
  plugins: {
    BrowserstackHelper: {
      require: 'codeceptjs-bshelper',
      user: process.env.BROWSERSTACK_USERNAME,
      key: process.env.BROWSERSTACK_ACCESS_KEY,
      shortUrl: false,
      enabled: (process.env.profile === 'undefined') ? false : true
    },
    pauseOnFail: {},
    retryFailedStep: {
      enabled: true
    },
    tryTo: {
      enabled: true
    },
    screenshotOnFail: {
      enabled: true
    }
  }
}